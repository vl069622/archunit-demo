package com.floordecor.archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.library.Architectures.layeredArchitecture;


public class LayerRulesTest {
    private final JavaClasses classes = new ClassFileImporter().importPackages("com.floordecor");

    @Test
    public void test_one_plus_two() {
        var one = 1;
        var two = 2;
        var result = 3;
        Assertions.assertEquals(result, one+two);
    }

    @Test
    public void layer_dependencies_are_respected() {
        layeredArchitecture()
                .layer("Controllers").definedBy("com.floordecor.archunit.controller..")
                .layer("Services").definedBy("com.floordecor.archunit.service..")
                .layer("Persistence").definedBy("com.floordecor.archunit.persistence..")

                .whereLayer("Controllers").mayNotBeAccessedByAnyLayer()
                .whereLayer("Services").mayOnlyBeAccessedByLayers("Controllers")
                .whereLayer("Persistence").mayOnlyBeAccessedByLayers("Services")

                .check(classes);
    }
}
