package com.floordecor.archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

public class InterfaceRulesTest {
    private final JavaClasses classes = new ClassFileImporter().importPackages("com.floordecor");

    @Test
    public void test_one_minus_one() {
        var one = 1;
        var result = 0;
        Assertions.assertEquals(result, one-one);
    }

    @Test
    public void interfaces_should_not_have_names_ending_with_the_word_interface() {
        noClasses().that().areInterfaces().should().haveNameMatching(".*Interface").check(classes);
    }

    @Test
    public void interfaces_should_not_have_simple_class_names_containing_the_word_interface() {
        noClasses().that().areInterfaces().should().haveSimpleNameContaining("Interface").check(classes);
    }

    @Test
    public void interfaces_must_not_be_placed_in_implementation_packages() {
        noClasses().that().resideInAPackage("..impl..").should().beInterfaces().check(classes);
    }
}