package com.floordecor.archunit.controller;

import com.floordecor.archunit.service.ArchunitService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class ArchunitController {
    private ArchunitService archunitService;

    public ArchunitController(ArchunitService archunitService) {
        this.archunitService = archunitService;
    }

    @GetMapping("/demo")
    public String demo() {
        return this.archunitService.demoService();
    }
}
