package com.floordecor.archunit.service;

import org.springframework.stereotype.Service;

@Service
public class ArchunitService {

    public String demoService() {
        return "demo archunit service";
    }
}
